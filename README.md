<details><summary>Start Steps</summary>
1. `npm install`
<br>
2. `npx tsc --init`
</details>

`docker-compose build`
<br>
`docker-compose up -d`
<br>

For get logs
<br>
`docker logs -f <container name>`
