import { fastify, FastifyInstance } from "fastify";
import pino from "pino";
import router from "./config/middleware/routers";
// import router from "@config/middleware/routers";
import * as dotenv from "dotenv";

dotenv.config();

const SERVER_PORT = process.env.SERVER_PORT!;
const SERVER_ADDRESS = process.env.SERVER_ADDRESS!;

const server = fastify({
  logger: pino({ level: "debug" }),
});

// Middleware: Router
server.register(router, {
  prefix: "/api/v1",
});

const start = async () => {
  try {
    await server.listen(SERVER_PORT, SERVER_ADDRESS);
    console.log(
      `Server listening on ${
        SERVER_ADDRESS != "0.0.0.0" ? SERVER_ADDRESS : "localhost"
      } port ${SERVER_PORT}`
    );
  } catch (error: any) {
    server.log.error(error);
    process.exit(1);
  }
};
start();
