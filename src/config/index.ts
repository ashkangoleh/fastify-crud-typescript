import { FastifyInstance } from "fastify";
import { FastifyPluginAsync, FastifyPluginOptions } from "fastify";
import fp from "fastify-plugin";
import mongoose from "mongoose";
import { Coin, CoinModel } from "./models/coinModel.js";
import { temp, TempModel } from "./models/temproryModel.js";
export interface Models {
  Coin: CoinModel;
  temp: TempModel;
}

export interface Db {
  models: Models;
}

// define options
export interface DBPluginOptions {
  uri: string;
}

const ConnectDB: FastifyPluginAsync<DBPluginOptions> = async (
  fastify: FastifyInstance,
  options: FastifyPluginOptions
) => {
  try {
    mongoose.connection.on("connected", () => {
      fastify.log.info({ actor: "MongoDB" }, "connected");
    });

    mongoose.connection.on("disconnected", () => {
      fastify.log.error({ actor: "MongoDB" }, "disconnected");
    });

    await mongoose.connect(options.uri, {});

    const models: Models = { Coin, temp };

    fastify.decorate("db", { models });
  } catch (error: any) {
    console.error(error);
  }
};

export default fp(ConnectDB);
