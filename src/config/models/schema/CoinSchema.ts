import { Static, Type } from "@sinclair/typebox";
import { RouteShorthandOptions } from "fastify";
import { CurrencyFormatter } from "../../utils/curr_formatter";

const DefaultCoinSc = Type.Object({
  name: Type.String(),
  market: Type.Number(),
  createdAt: Type.String(),
  updatedAt: Type.String(),
  IRR_market_amount: Type.Optional(Type.String()),
  USD_market_amount: Type.Optional(Type.String()),
});
const SingleData = Type.Object({
  name: Type.String(),
  details: Type.Object({
    market: Type.Number(),
    IRR_market_amount: Type.Optional(Type.String()),
    USD_market_amount: Type.Optional(Type.String()),
  }),
});
const Querystring = Type.Object({
  name: Type.String(),
});

const GetAllResponse = Type.Object({
  status: Type.String(),
  data_count: Type.Optional(Type.String()),
  data: Type.Array(DefaultCoinSc),
});
const GetSingleResponse = Type.Object({
  status: Type.String(),
  data: SingleData,
});
const PostAllResponse = Type.Object({
  status: Type.String(),
  data: DefaultCoinSc,
});
const ExistingData = Type.Object({
  status: Type.String(),
  message: Type.String(),
});
const Params = Type.Object({
  name: Type.String(),
});

type Query = Static<typeof Querystring>;
type QueryDefaultCoinSc = Static<typeof DefaultCoinSc>;
type Param = Static<typeof Params>;

export interface CoinRequest {
  // Querystring: Query;
  Querystring: QueryDefaultCoinSc;
  Params: Param;
}

export const CoinOpts: RouteShorthandOptions = {
  schema: {
    response: {
      200: GetAllResponse,
      201: PostAllResponse,
      404: ExistingData,
    },
  },
};
export const CoinSingleOpts: RouteShorthandOptions = {
  schema: {
    params: Params,
    response: {
      200: GetSingleResponse,
      404: ExistingData,
    },
  },
};

export const CoinSingleDelete: RouteShorthandOptions = {
  schema: {
    body: Querystring,
    response: {
      200: {
        type: "object",
        properties: {
          status: { type: "string" },
          data: { type: "string" },
          message: { type: "string" },
        },
      },
      404: ExistingData,
    },
  },
};
