import { Static, Type } from "@sinclair/typebox";
import { RouteShorthandOptions } from "fastify";

const defaultTempSchema = Type.Object({
  name: Type.String(),
  description: Type.String(),
  createdAt: Type.String(),
  updatedAt: Type.String(),
});
const Params = Type.Object({
  name: Type.String(),
});
type QueryTempSchema = Static<typeof defaultTempSchema>;
type Param = Static<typeof Params>;

export interface TempRequest {
  Querystring: QueryTempSchema;
  Params: Param;
}

const GetAllResponse = Type.Object({
  status: Type.String(),
  data_count: Type.Optional(Type.String()),
  data: Type.Array(defaultTempSchema),
});
const GetSingleResponse = Type.Object({
  status: Type.String(),
  data: defaultTempSchema,
});

const PostSingleTempOpts = Type.Object({
  status: Type.String(),
  data: defaultTempSchema,
});

export const TempOpts: RouteShorthandOptions = {
  schema: {
    response: {
      200: defaultTempSchema,
      201: PostSingleTempOpts,
    },
  },
};

export const GetSingleTempOpts: RouteShorthandOptions = {
  schema: {
    response: {
      200: GetSingleResponse,
    },
  },
};
