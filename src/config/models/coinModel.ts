import {Schema, Document, model, Model} from "mongoose";

export interface CoinAttrs {
    name: string;
    market: number;
    IRR_market_amount?: string;
    USD_market_amount?: string;
}

export interface CoinDocument extends Document {
    name: string;
    market: number;
    createdAt: string;
    updatedAt: string;
    IRR_market_amount?: string;
    USD_market_amount?: string;
}



export interface CoinModel extends Model<CoinDocument> {
  addOne(doc: CoinAttrs): CoinDocument;
}

export const coinSchema:Schema= new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        market: {
            type: Number,
            required: true,
        },
        IRR_market_amount: {
            type: String,
        },
        USD_market_amount: {
            type: String,
        },
    },
    {
        timestamps: true,
    }
);

coinSchema.statics.addOne = (doc: CoinAttrs) => {
    return new Coin(doc);
};

export const Coin = model<CoinDocument, CoinModel>("Coin", coinSchema);
