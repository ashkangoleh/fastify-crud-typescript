// import { Schema, Document, model, Model } from "mongoose";
import { coinSchema } from "./coinModel";
import * as mongo from 'mongoose'
export interface TempAttrs {
  name: string;
  description: string;
  coin:object
}

export interface TempDocument extends mongo.Document {
  name: string;
  description: string;
  coin:object;
}

export interface TempModel extends mongo.Model<TempDocument> {
  addOne(doc: TempAttrs): TempDocument;
}

export const TempSchema: mongo.Schema = new mongo.Schema(
  {
    name: {
      type: "string",
      required: true,
    },
    description: {
      type: "string",
      required: true,
    },
    coin: coinSchema,
  },
  {
    timestamps: true,
  }
);

TempSchema.statics.addOne = (doc: TempAttrs) => {
  return new temp(doc);
};
export const temp = mongo.model<TempDocument, TempModel>("Temp", TempSchema);
