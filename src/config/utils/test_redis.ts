import { Client } from "../middleware/redis";
import * as redis from "redis";
import { Db } from "../../config/index";
import { Coin } from "../models/coinModel";
import { temp } from "../models/temproryModel";
export const client_test = redis.createClient(6379, "localhost");
client_test.on("error", function (error) {
  console.error(error);
});

export async function test_show() {
  try {
    const Coin_Cache: any = await Coin.find({ $exists: true });
    const Temp_Cache: any = await temp.find({ $exists: true });
    Temp_Cache.forEach((Element: any) => {
      let data = {
        name: Element["name"],
        description: Element["description"],
        coin: Coin_Cache,
      };
      Client.set(`crypto:name:${Element["name"]}`, JSON.stringify(data));
    });
    Coin_Cache.forEach((Element: any) => {
      let data = {
        market: `${Element["market"]}`,
        IRR_market_amount: `${Element["IRR_market_amount"]}`,
        USD_market_amount: `${Element["USD_market_amount"]}`,
      };
      Client.set(`crypto:coin:${Element["name"]}`, JSON.stringify(data));
    });
  } catch (error) {
    console.log(error);
  }
}
