import axios from "axios";

export async function CurrencyFormatter(currency: string, digits: number) {
  let formatter: any = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: `${currency}`,
  });
  if (currency == "USD") {
    return formatter.format(digits);
  } else if (currency == "IRR") {
    let converter = (
      await axios.get("https://raters.ir/exchange/api/currency/usd")
    ).data;
    let IRRPrice: any = converter["data"]["prices"][0]["max"];
    IRRPrice = IRRPrice.split(",").join("");
    let IRRConverted: number = digits * parseInt(IRRPrice);
    return formatter.format(IRRConverted);
  } else {
    return "currency not found";
  }
}
