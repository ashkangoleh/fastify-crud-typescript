import * as redis from "redis";

export const Client = redis.createClient(6378, "localhost");
Client.on("error", function (error) {
  console.error(error);
});
export default redis;
