import { FastifyInstance } from "fastify";
import db from "../index";
import CoinRoute from "../../controllers/coinController";
import * as dotenv from "dotenv";
import fastifySwagger from "fastify-swagger";
import TempRoute from "../../controllers/TempController"
dotenv.config();

const DB_PORT = process.env.MONGODB_PORT!;
const DB_ADDRESS = process.env.MONGODB_ADDRESS!;

const uri = `mongodb://${DB_ADDRESS}:${DB_PORT}/arz`;
// const uri = `mongodb://mongo:27017/arz`;

export default async function router(fastify: FastifyInstance) {
  fastify.register(CoinRoute);
  fastify.register(TempRoute);
  fastify.register(db, { uri });
}

