import { Db } from "../config";
// import { Db } from "@root/config";
import { TempOpts } from "../config/models/schema/TempSchema";
// import { TempOpts } from "@config/models/schema/TempSchema";
import { TempAttrs } from "../config/models/temproryModel";
// import { TempAttrs } from "@root/config/models/temproryModel";
import {
  FastifyPluginAsync,
  FastifyInstance,
  FastifyPluginOptions,
} from "fastify";
import fp from "fastify-plugin";
import { test_show } from "../config/utils/test_redis";

declare module "fastify" {
  export interface FastifyInstance {
    db: Db;
  }
}

const TempRoute: FastifyPluginAsync = async (
  server: FastifyInstance,
  options: FastifyPluginOptions
) => {

  server.post<{ Body: TempAttrs }>("/temp", TempOpts, async (req, reply) => {
    try {
      let { name, description } = req.body;

      const { temp, Coin } = server.db.models;
      const filter = await temp.find({ name: name });
      const coins: any = await Coin.findOne();

      if (!filter.length) {
        const Temp = temp.addOne({
          name: name,
          description: description,
          coin: coins,
        });
        await Temp.save();
        return reply.code(201).send({
          status: "success",
          data: Temp,
        });
      } else {
        return reply.code(409).send({
          status: "error",
          message: `'${name}\' is already exists`,
        });
      }
    } catch (error: any) {
      reply.status(500).send(error);
    }
  });
};
export default fp(TempRoute);
