import {
    FastifyInstance,
    FastifyPluginOptions,
    FastifyPluginAsync,
} from "fastify";
import fp from "fastify-plugin";

import {Db} from "../config";
// import { Db } from "@root/config";
import {CoinAttrs} from "../config/models/coinModel";
// import { CoinAttrs } from "@config/models/coinModel";


import {
    CoinOpts,
    CoinRequest,
    CoinSingleDelete,
    CoinSingleOpts,
} from "../config/models/schema/CoinSchema";
// } from "@root/config/models/schema/CoinSchema";

import {CurrencyFormatter} from "../config/utils/curr_formatter";
// import { CurrencyFormatter } from "@config/utils/curr_formatter";
import {Client} from "../config/middleware/redis";
// import { Client } from "@config/middleware/redis";
import {test_show, client_test} from "../config/utils/test_redis";
// import { test_show, client_test } from "@config/utils/test_redis";
import pino from 'pino';

const logger = pino({
})
// Declaration merging
declare module "fastify" {
    export interface FastifyInstance {
        db: Db;
    }
}

interface coinParams {
    name: string;
}

// export declare interface globalKeys {
//   [key: string]: any;
// }
const CoinRoute: FastifyPluginAsync = async (
    server: FastifyInstance,
    options: FastifyPluginOptions
) => {
    // server.addHook("onReady", async () => {
    //   // each time server is ready or reset for first time using onReady otherwise using onRequest
    //   return test_show();
    // });
    server.addHook("onRequest", async (req, reply) => {
        // log.info(req)
        logger.info("asdads")
        return test_show();
    });
    //get all data
    server.get<CoinRequest>("/coins", CoinOpts, async (request, response) => {
        try {
            const {Coin, temp} = server.db.models;
            const coins: any = await Coin.find({exists: true}, {_id: 0, __v: 0});
            if (coins.length) {
                return response.code(200).send({
                    status: "success",
                    data_count: coins.length,
                    data: coins,
                });
            } else {
                return response.code(404).send({
                    status: "error",
                    message: "not found",
                });
            }
        } catch (error) {
            return response.send({
                status: "error",
                message: error,
            });
        }
    });
    //get single data under 7k or over 7k
    server.get<CoinRequest>("/coins/lte", CoinOpts, async (request, response) => {
        try {
            const {Coin} = server.db.models;
            const coins = await Coin.find().where("market").lte(7000);
            if (coins.length) {
                return response.code(200).send({
                    status: "success",
                    data_count: coins.length,
                    data: coins,
                });
            } else {
                return response.code(404).send({
                    status: "error",
                    message: "not found",
                });
            }
        } catch (error) {
            return response.send({
                status: "error",
                message: request.log.error(error),
            });
        }
    });
    server.post<{ Body: CoinAttrs }>(
        "/coins",
        CoinOpts,
        async (request, response) => {
            try {
                const {Coin} = server.db.models;
                let {name, market} = request.body;
                name = name.toUpperCase();
                let IRR_amount = await CurrencyFormatter("IRR", market);
                let USD_amount = await CurrencyFormatter("USD", market);
                const existCoin = await Coin.exists({name: name});
                if (!existCoin) {
                    const coin = await Coin.addOne({
                        name: name,
                        market: market,
                        IRR_market_amount: IRR_amount,
                        USD_market_amount: USD_amount,
                    });
                    await coin.save();
                    return response.code(201).send({
                        status: "success",
                        data: coin,
                    });
                } else {
                    return response.code(404).send({
                        status: "error",
                        message: `'${name}\' is already exist`,
                    });
                }
            } catch (error) {
                return response.send({
                    status: "error",
                    message: request.log.error(error),
                });
            }
        }
    );
    server.get<{ Params: coinParams }>(
        "/coins/:name",
        CoinSingleOpts,
        async (request, response) => {
            try {
                let name = request.params.name;
                name = name.toUpperCase();
                //////////////////get data from redis
                Client.keys("*", (err: any, key: any) => {
                    key.filter((_key: any, index: any, array: any) => {
                        if (_key.includes(name)) {
                            Client.get(_key, (err, value) => {
                                const coin: any = value;
                                if (!coin) {
                                    return response.send(404);
                                }
                                return response.code(200).send({
                                    status: "success",
                                    data: {
                                        name: `${name}`,
                                        details: JSON.parse(coin),
                                    },
                                });
                            });
                        }
                    });
                });
            } catch (error: any) {
                return response.send({
                    status: "error",
                    message: request.log.error(error),
                });
            }
        }
    );
    server.delete<{ Body: CoinAttrs }>(
        "/coins",
        CoinSingleDelete,
        async (request, response) => {
            try {
                const {Coin} = server.db.models;
                let {name, market} = request.body;
                name = name.toUpperCase();
                const coin = await Coin.findOne({name: name});
                if (coin) {
                    await coin.delete();
                    return response.code(200).send({
                        status: "success",
                        data: name,
                        message: "Deleted",
                    });
                } else {
                    return response.code(404).send({
                        status: "error",
                        message: `'${name}\' not found`,
                    });
                }
            } catch (error) {
                return response.send({
                    status: "error",
                    message: request.log.error(error),
                });
            }
        }
    );
    server.patch<{ Body: CoinAttrs }>("/coins", {}, async (request, response) => {
        try {
            const {Coin} = server.db.models;
            let {name, market} = request.body;
            name = name.toUpperCase();
            let SelectedCoin = await Coin.findOne({name: name});
            if (SelectedCoin) {
                let UpdatedCoin = await Coin.findOneAndUpdate(
                    {name: name},
                    {
                        market: market,
                        USD_market_amount: await CurrencyFormatter("USD", market),
                        IRR_market_amount: await CurrencyFormatter("IRR", market),
                    },
                    {new: true}
                );

                return response.status(201).send({
                    status: "success",
                    data: {
                        name: `Current coin: ${name}`,
                        IRR_market_amount: `${await CurrencyFormatter("IRR", market)}`,
                        USD_market_amount: `${await CurrencyFormatter("USD", market)}`,
                    },
                    message: "Updated",
                });
            } else {
                return response.code(404).send({
                    status: "error",
                    message: "not found",
                });
            }
        } catch (error) {
            return response.send({
                status: "error",
                message: request.log.error(error),
            });
        }
    });
    server.get("/time", async (req, res) => {
        try {
            client_test.keys("*", async (err, result) => {
                const list: any = [];
                result.filter((value: string, index, array) => {
                    if (
                        value.includes(
                            // `crypto:matches:timeframe:${exchange}:${timeframe}:${name}`
                            `crypto:matches:timeframe:binance:5:btc`
                        )
                    ) {
                        list.push(value);
                    }
                });
                return res.code(200).send({
                    status: "success",
                    data: list,
                });
            });
        } catch (error: any) {
            return res.code(500).send(error);
        }
    });
};
export default fp(CoinRoute);
