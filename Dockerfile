FROM node:14-alpine as base

WORKDIR /home/node/app/

COPY package*.json /home/node/app/

RUN npm i

COPY . /home/node/app/


FROM base as production

ENV NODE_PATH=./build

RUN npm run build
